from datetime import datetime, timedelta
from typing import List, Optional

from sqlmodel import Relationship, Field, SQLModel, create_engine


class Message(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    created_at: datetime = Field(default_factory=datetime.now(), nullable=False)
    status: str
    mailing_id: Optional[int] = Field(default=None, foreign_key="mailing.id") 
    client_id: Optional[int] = Field(default=None, foreign_key="client.id") 

class Mailing(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    created_at: datetime = Field(default_factory=datetime.now(), nullable=False)
    text: str
    clients: List["Client"] = Relationship(
        back_populates="mails", link_model=Message
    ) 
    end_time: datetime = Field(default_factory=datetime.now(), nullable=True)

class Client(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    phone: str
    code: str
    tag: str
    timezone: str
    mails: List[Mailing] = Relationship(
        back_populates="clients", link_model=Message
    )


