from fastapi import FastAPI
from fastapi.params import Depends
from sqlmodel import Session, select

from models import *
from db import get_session, init_db

app = FastAPI()


@app.on_event("startup")
def on_startup():
    init_db()


@app.get("/")
def hello():
    return {"Hello!"}


@app.post("/add_client")
def add_client(client: Client, session: Session = Depends(get_session)):
    client = Client(
        phone=client.phone, 
        code=client.code, 
        tag=client.tag, 
        timezone=client.timezone
    )
    session.add(client)
    session.commit()
    session.refresh(client)


@app.post("/update_client")
def update_client(
    client_id: int, 
    phone: str,
    code: str,
    tag: str,
    timezone: str, 
    session: Session = Depends(get_session)):
    results = session.exec(select(Client).where(Client.id == client_id))
    client = results.one()
    client.phone = phone
    client.code = code
    client.tag = tag
    client.timezone = timezone
    session.add(client)
    session.commit()
    session.refresh(client)


@app.post("/delete_client")
def delete_client(client_id: int, session: Session = Depends(get_session)):
    results = session.exec(select(Client).where(Client.id == client_id))
    client = results.one()
    session.delete(client)
    session.commit()


@app.post("/add_mailing")
def add_mailing(mailing: Mailing, session: Session = Depends(get_session)):
    mailing = Mailing( 
        text=mailing.text, 
        created_at=mailing.created_at, 
        clients=mailing.clients, 
        end_time=mailing.end_time
    )
    session.add(mailing)
    session.commit()
    session.refresh(mailing)

"""
def test(session = next(get_session())):
    query = session.execute(select(Mailing).where(Mailing.id == 1))
    q = query.first()
    print(q.mails)

test()
"""
